#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <papi.h>

int n,xi,yi,xj,yj;

void entrada(int rank, int cnn);
void imprimir(int a[n][n]);
void coordenadas(int a[n][n], int n);

int main(void){
    int retval, EventSet = PAPI_NULL;
    long_long start_cycles, end_cycles, start_usec, end_usec;
    unsigned int native = 0x0;
    PAPI_event_info_t info;
    long_long values[2] = {(long_long) 0};

    int i,j,k;
    int x;
    int pasa = 0;
    int rank, cnn;
    int c = 0;

    retval = PAPI_library_init(PAPI_VER_CURRENT);
    if (retval != PAPI_VER_CURRENT) {
        printf("PAPI library init error!\n");
        exit(1);
    }

    //inicio MPI
    MPI_Init(NULL,NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &cnn);

    //tarea 1 P0
    if(rank == 0){
        entrada(rank, cnn);
    }

    /* Create an EventSet */
    retval = PAPI_create_eventset(&EventSet);
    if (retval != PAPI_OK){
        printf("PAPI create error!\n");
        exit(1);
    }

    /* Add Total Instructions Executed to our EventSet*/ 
    retval = PAPI_add_event(EventSet, PAPI_TOT_INS);
    retval = PAPI_add_event(EventSet, PAPI_TOT_CYC);
    /*
    retval = PAPI_add_event(EventSet, PAPI_VEC_DP);
    retval = PAPI_add_event(EventSet, PAPI_FP_OPS);*/

    if (retval != PAPI_OK){
        printf("PAPI add instruction error!\n");
        exit(1);
    }

    /* Start counting */
    retval = PAPI_start(EventSet);
    if (retval != PAPI_OK) {
        printf("PAPI start error!\n");
        exit(1);
    }

    if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT)
    exit(1);

//-----------------------------------------------------------------------------

    MPI_Recv(&n, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    int dist [n][n];
    int nAux = n*n;

    //tarea 2 P1
    if(rank == 1)
    {
        for(i = 0; i < n; ++i){
            for(j = 0; j < n; ++j){
                if (i == j){
                    dist[i][j] = 0;
                }else{
                    x = rand() % 100;

                    //printf("%2.f\n", x);
                    if (x > 90 && x < 100 ){
                        dist[i][j]= 100000;    
                    }else{
                        dist[i][j]= x;
                    }
                }
            }
        }
        MPI_Send(&dist, nAux, MPI_INT, 2, 0,MPI_COMM_WORLD);
    }
    

    //tarea 3
    if(rank == 2){
        MPI_Recv(&dist, nAux, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        imprimir(dist);
        printf("\n");
        for(k = 0; k < n; k++){
            for(i = 0; i < n; i++){
                for(j = 0; j < n; j++){
                    int dt = dist[i][k] + dist[k][j];
                    if(dist[i][j] == 100000){
                        dist[i][j] = dt;
                    }
                }
            }
        }
        imprimir(dist);
        coordenadas(dist,n);  
    }

    //tarea 4

    if(rank == 3){
        MPI_Recv(&xi, 1, MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&xj, 1, MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&yi, 1, MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&yj, 1, MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        printf("Camino: ");
        i = xi;
        j = yi;

        while(i != xj || j != yj){
            printf("%d, ",dist[i][j]);

            if (i < xj){
                i++;
            }else if (i > xj){
                i--;
            }
            
            if (j < yj){
                j++;
            }else if (j > yj){
                j--;
            }
        }
    printf("%d\n",dist[i][j]);
    
        
//-------------------------------------------------------------------------


        /*salida .json area para obtener .json
    retvalArea = PAPI_hl_region_end("MPI_output");*/

     /* Gets the ending time in clock cycles */
    end_cycles = PAPI_get_real_cyc();

    /* Gets the ending time in microseconds */
    end_usec = PAPI_get_real_usec();

    printf("\n\nRealización de prueba de latencia de ejecuciòn...\n\n");
    printf("ciclos de reloj: %lld\n", end_cycles - start_cycles);
    printf("tiempo en microsegundos: %lld\n\n", end_usec - start_usec); 

    /* Read the counters */
    retval = PAPI_read(EventSet, values);
    if (retval != PAPI_OK){
        printf("PAPI read counters error!\n");
        exit(1);
    }
    printf("********************\n");
    
    printf("PAPI_TOT_INS - Después de leer los contadores: %lld\n",values[0]);
    printf("PAPI_TOT_CYC - Después de leer los contadores: %lld\n",values[1]);
    /*
    printf("PAPI_VEC_SP - After reading counters: %lld\n",values[2]);
    printf("PAPI_FP_OPS - After reading counters: %lld\n",values[3]);*/

    printf("\n");

    /* Start the counters */
    retval = PAPI_stop(EventSet, values);
    if (retval != PAPI_OK) {
        printf("PAPI stop instruction error!\n");
        exit(1);
    }
    //printf("********** counts **********", rank);
    
    printf("PAPI_TOT_INS - Después de detener los contadores: %lld\n",values[0]);
    printf("PAPI_TOT_CYC - Después de detener los contadores: %lld\n",values[1]);
    /*
    printf("PAPI_VEC_SP - After stopping counters: %lld\n",values[2]);
    printf("PAPI_FP_OPS - After stopping counters: %lld\n",values[3]);*/

    }
    
    MPI_Finalize();
    return 0;
}

void entrada(int rank, int cnn){    
    int i;
    do{
        printf("ingrese n: \n");
        scanf("%d", &n);
    }while(n < 300 || n > 400);
    //}while(n > 400);
    //printf("ingreso correcto: %d \n", n);
    //envia resultado de tarea 1 a p1

    for (i = 0; i < cnn; ++i){
        MPI_Send(&n, 1, MPI_INT, i, 0,MPI_COMM_WORLD);
    }
}

void coordenadas(int a[n][n], int n){    
    do{
        printf("Ingrese las coordenadas de origen: \n");
        scanf("%d %d", &yi,&xi);
        printf("Ingrese las coordenadas de destino: \n");
        scanf("%d %d", &yj,&xj);

    }while(xi > n || xj > n || yi > n || yj > n || xi < 0 || xj < 0 || yi < 0 || yj < 0);

    printf("Origen [%d,%d]: %d\n",yi,xi, a[xi][yi]);
    printf("Destino [%d,%d]: %d\n",yj,xj, a[xj][yj]);

    MPI_Send(&xi, 1, MPI_INT, 3, 0,MPI_COMM_WORLD);
    MPI_Send(&xj, 1, MPI_INT, 3, 0,MPI_COMM_WORLD);
    MPI_Send(&yi, 1, MPI_INT, 3, 0,MPI_COMM_WORLD);
    MPI_Send(&yj, 1, MPI_INT, 3, 0,MPI_COMM_WORLD);
    
}

void imprimir(int a[n][n]){
    int x,y;
    for (x = 0; x < n; x++){
        for (y = 0; y < n; y++){
            printf("%d\t", a[x][y]);
        }   
        printf("\n");
    }
}

